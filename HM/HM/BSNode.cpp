#include "BSNode.h"

#include <iostream>
#include <math.h>


BSNode::BSNode(const std::string& data) : _data(data)
{
    // no children
    _left = nullptr;
    _right = nullptr;
    _count = 0;
}

BSNode::BSNode(const BSNode& other)
{
    // same data
    _data = other.getData();
    // left may be null or new node (recursion)
    _left = other.getLeft() == nullptr ? nullptr: new BSNode(*other.getLeft());
    // same 
    _right = other.getRight() == nullptr ? nullptr: new BSNode(*other.getRight());
}

BSNode::~BSNode()
{
    // delete recursivily
    if(_left != nullptr)
        delete _left;
    if(_right != nullptr)
        delete _right;
}

void BSNode::insert(const std::string& value)
{

    // inc the counter if the item appear
    if (value == getData())
    {
        _count++;
        return;
    }

    // insert the item to the right
    if (value > getData() && getRight() == nullptr)
    {
        _right = new BSNode(value);
        return;
    }
    // insert the item to the left
    if (value < getData() && getLeft() == nullptr)
    {
        _left = new BSNode(value);
        return;
    }
    // move to the left sub-tree   
    if (value < getData())
        getLeft()->insert(value);

    // move to the right sub-tree
    if (value > getData())
        getRight()->insert(value); 
    
}

BSNode& BSNode::operator=(const BSNode& other)
{
    // same data
    _data = other.getData();

    // same as copy C'tor
    _left = other.getLeft() == nullptr ? nullptr : new BSNode(*other.getLeft());
    _right = other.getRight() == nullptr ? nullptr : new BSNode(*other.getRight());

    return *this;
}

bool BSNode::isLeaf() const
{
    return getLeft() == nullptr && getRight() == nullptr;
}

std::string BSNode::getData() const
{
    return _data;
}

BSNode* BSNode::getLeft() const
{
    return _left;
}

BSNode* BSNode::getRight() const
{
    return _right;
}

int BSNode::getCount() const
{
    return _count;
}

bool BSNode::search(const std::string& val) const
{
    // fund the val
    if (getData() == val)
        return true;
    // search in the left sub-tree
    if (getData() > val && getLeft() != nullptr)
        return getLeft()->search(val);
    // search in the right sub-tree
    if (getData() < val && getRight() != nullptr)
        return getRight()->search(val);

    // no path to the value (not exist)
    return false;
}

int BSNode::getHeight() const
{
    // no children
    if (getLeft() == nullptr && getRight() == nullptr)
        return 0; //this node (tree of 2 nodes have depth of one)
    // one child
    if (getLeft() == nullptr && getRight() != nullptr)
        return 1 + getRight()->getHeight();
    // one child
    if (getLeft() != nullptr && getRight() == nullptr)
        return 1 + getLeft()->getHeight();

    return 1 + std::max(getLeft()->getHeight(), getRight()->getHeight());
}

int BSNode::getDepth(const BSNode& root) const
{
    int ans = -1;

    if (root.search(getData()))
        ans = getCurrNodeDistFromInputNode(new BSNode(root));

    return ans;
}

void BSNode::printNodes()
{
    if (getLeft() != nullptr)
        getLeft()->printNodes();

    std::cout << getData() << " " << getCount() << std::endl;

    if (getRight() != nullptr)
        getRight()->printNodes();
}

int BSNode::getCurrNodeDistFromInputNode(BSNode* node) const
{
    if (this->getData() == node->getData())
        return 0;

    if (node->getLeft() != nullptr && getData() < node->getData())
        return 1 + getCurrNodeDistFromInputNode(node->getLeft());

    if (node->getRight() != nullptr && getData() > node->getData())
        return 1 + getCurrNodeDistFromInputNode(node->getRight());
    return -1;
}
