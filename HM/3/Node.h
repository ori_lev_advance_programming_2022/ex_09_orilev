#pragma once

#include <string>
#include <iostream>
#include <math.h>


template <class T>
class Node
{
public:
	Node(const T data);
	Node(const Node& other);

	~Node();

	void insert(const T value);
	Node& operator=(const Node& other);

	bool isLeaf() const;
	T getData() const;
	Node<T>* getLeft() const;
	Node<T>* getRight() const;
	int getCount() const;
	// not const, need to point to this ptr
	bool search(const T val) const;

	int getHeight() const;
	// not const (using search)
	int getDepth(const Node<T>& root) const;

	void printNodes();

private:
	T _data;
	Node* _left;
	Node* _right;

	int _count; //to count replicates
	int getCurrNodeDistFromInputNode(Node* node) const; //auxiliary function for getDepth

};

template <class T>
Node<T>::Node(const T data) : _data(data)
{
	// no children
	_left = nullptr;
	_right = nullptr;
	_count = 0;
}


template <class T>
void Node<T>::insert(const T value)
{

	// inc the counter if the item appear
	if (value == getData())
	{
		_count++;
		return;
	}

	// insert the item to the right
	if (value > getData() && getRight() == nullptr)
	{
		_right = new Node(value);
		return;
	}
	// insert the item to the left
	if (value < getData() && getLeft() == nullptr)
	{
		_left = new Node(value);
		return;
	}
	// move to the left sub-tree   
	if (value < getData())
		getLeft()->insert(value);

	// move to the right sub-tree
	if (value > getData())
		getRight()->insert(value);

}

template <class T>
T Node<T>::getData() const
{
	return _data;
}


template <class T>
bool Node<T>::search(const T val) const
{
	// fund the val
	if (getData() == val)
		return true;
	// search in the left sub-tree
	if (getData() > val && getLeft() != nullptr)
		return getLeft()->search(val);
	// search in the right sub-tree
	if (getData() < val && getRight() != nullptr)
		return getRight()->search(val);

	// no path to the value (not exist)
	return false;
}

template <class T>
Node<T>::Node(const Node& other)
{
	// same data
	_data = other.getData();
	// left may be null or new node (recursion)
	_left = other.getLeft() == nullptr ? nullptr : new Node(*other.getLeft());
	// same 
	_right = other.getRight() == nullptr ? nullptr : new Node(*other.getRight());
}

template <class T>
Node<T>::~Node()
{
	// delete recursivily
	if (_left != nullptr)
		delete _left;
	if (_right != nullptr)
		delete _right;
}


template <class T>
Node<T>& Node<T>::operator=(const Node<T>& other)
{
	// same data
	_data = other.getData();

	// same as copy C'tor
	_left = other.getLeft() == nullptr ? nullptr : new Node(*other.getLeft());
	_right = other.getRight() == nullptr ? nullptr : new Node(*other.getRight());

	return *this;
}

template <class T>
bool Node<T>::isLeaf() const
{
	return getLeft() == nullptr && getRight() == nullptr;
}


template <class T>
Node<T>* Node<T>::getLeft() const
{
	return _left;
}

template <class T>
Node<T>* Node<T>::getRight() const
{
	return _right;
}

template <class T>
int Node<T>::getCount() const
{
	return _count;
}

template <class T>
int Node<T>::getHeight() const
{
	// no children
	if (getLeft() == nullptr && getRight() == nullptr)
		return 0; //this node (tree of 2 nodes have depth of one)
	// one child
	if (getLeft() == nullptr && getRight() != nullptr)
		return 1 + getRight()->getHeight();
	// one child
	if (getLeft() != nullptr && getRight() == nullptr)
		return 1 + getLeft()->getHeight();

	return 1 + std::max(getLeft()->getHeight(), getRight()->getHeight());
}

template <class T>
int Node<T>::getDepth(const Node& root) const
{
	int ans = -1;

	if (root.search(getData()))
		ans = getCurrNodeDistFromInputNode(new Node(root));

	return ans;
}

template <class T>
void Node<T>::printNodes()
{
	if (getLeft() != nullptr)
		getLeft()->printNodes();

	std::cout << getData() << " " << getCount() << std::endl;

	if (getRight() != nullptr)
		getRight()->printNodes();
}

template <class T>
int Node<T>::getCurrNodeDistFromInputNode(Node<T>* node) const
{
	if (this->getData() == node->getData())
		return 0;

	if (node->getLeft() != nullptr && getData() < node->getData())
		return 1 + getCurrNodeDistFromInputNode(node->getLeft());

	if (node->getRight() != nullptr && getData() > node->getData())
		return 1 + getCurrNodeDistFromInputNode(node->getRight());
	return -1;
}
