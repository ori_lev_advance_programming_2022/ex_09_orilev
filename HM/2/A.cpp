#include "A.h"


A::A()
{
	_a = 5;
}

A::~A()
{
}

int A::getA() const
{
	return _a;
}

void A::setA(int a)
{
	_a = a;
}

bool A::operator==(const A& other) const
{
	return _a == other.getA();
}

bool A::operator<(const A& other) const
{
	return _a < other.getA();
}

std::ostream& operator<<(std::ostream& os, const A& a)
{
	os << a.getA();
	return os;
}
