#include "Node.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>

template <class T>
void printArr(T arr[], int size)
{
	int i = 0;
	
	for (i = 0; i < size; i++)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}


int main()
{
	int i = 0;

	int arr[] = { 7, 4, 11, 2, 13, 1, 15, 3, 12, 14, 10, 9, 8, 6 };
	std::string arr_s[] = { "f", "a", "c", "i", "j", "b", "h", "e", "z", "q", "p", "o", "k", "l"};

	std::cout << "int array: ";
	printArr(arr, 14);

	std::cout << "int string: ";
	printArr(arr_s, 14);

	Node<int>* bs = new Node<int>(arr[0]);
	Node<std::string>* bstring = new Node<std::string>(arr_s[0]);
	
	for (i = 1; i < 14; i++)
		bs->insert(arr[i]);

	for (i = 1; i < 14; i++)
		bstring->insert(arr_s[i]);
	
	bs->printNodes();

	std::cout << "------string-----" << std::endl;

	bstring->printNodes();
	delete bs;
	delete bstring;

	return 0;
}

