#pragma once

#include <string>

class BSNode
{
public:
	BSNode(const std::string& data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(const std::string& value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	std::string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;
	int getCount() const;
	// not const, need to point to this ptr
	bool search(const std::string& val) const;

	int getHeight() const;
	// not const (using search)
	int getDepth(const BSNode& root) const;

	void printNodes();

private:
	std::string _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //to count replicates
	int getCurrNodeDistFromInputNode(BSNode* node) const; //auxiliary function for getDepth

};
