#pragma once

#include <iostream>

class A {
public:
	A();
	~A();

	int getA() const;
	void setA(int a);

	bool operator==(const A& other) const;
	bool operator<(const A& other) const;

	friend std::ostream& operator<<(std::ostream& os, const A& a);
private:
	int _a;
};