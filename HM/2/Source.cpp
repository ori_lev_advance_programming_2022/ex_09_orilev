#include <iostream>
#include "A.h"


template <class T>
int compare(T a, T b)
{
	if (a == b)
		return 0;
	if (a < b)
		return 1;
	return -1;
}

template <class T>
void bubbleSort(T arr[], int size)
{
	int i = 0, j = 0;
	T temp;
	for (i = 0; i < size - 1; i++)
	{
		for (j = 0; j < size - i - 1; j++)
		{
			if (compare(arr[j + 1], arr[j]) == 1)
			{
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
				
		}
	}
}


template <class T>
void printArray(T arr[], int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
		std::cout << arr[i] << std::endl;
}


int main()
{
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	A a = A();
	A b = A();
	A c = A();

	b.setA(4);
	c.setA(6);

	A a_lst[3] = { c, a, b };

	printArray<A>(a_lst, 3);
	bubbleSort<A>(a_lst, 3);
	printArray<A>(a_lst, 3);
	system("pause");

	return 0;
}
